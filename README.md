# Cell Colony Computer Vision

Exploratory project for applying computer vision machine learning to microscope captured images of cells with the primary purpose of counting cells.

## View Project Notebook

URL:
https://gitlab.com/rsmbg/cell-colony-computer-vision.git

View with binder:
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Frsmbg%2Fcell-colony-computer-vision.git/master

View with nbviewer:
https://nbviewer.jupyter.org/urls/gitlab.com/rsmbg/cell-colony-computer-vision/-/raw/4ac661ba379bda41c2c9139b555021d6f3604b9c/Cell%20Colony%20Computer%20Vision.ipynb

